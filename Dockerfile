from ubuntu:bionic-20190912.1
ADD package_indexer /

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install python3 -y

CMD ["python3", "package_indexer.py"]
