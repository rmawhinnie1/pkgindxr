import pytest

host = '127.0.0.1'
port = 8080
cmd = '\n'
wait_time = 0

def connect(host, port, cmd='\n', wait_time=0):
    import socket
    from time import sleep

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((host, port))
        s.sendall(cmd.encode())
        sleep(wait_time)
        data = s.recv(1024)
        return data.decode('UTF-8')
    except:
        return False

'''The server will open a TCP socket on port 8080.'''
def test_listening():
    assert connect('127.0.0.1', 8080, '\n', 0) != False

'''It must accept connections from multiple clients at the same time,'''
def test_muliple_connections():
    from concurrent.futures import ThreadPoolExecutor

    executors_list = []
    results = []
    with ThreadPoolExecutor(max_workers=5) as executor:
        executors_list.append(executor.submit(connect, '127.0.0.1', 8080, '\n', 2))
        executors_list.append(executor.submit(connect, '127.0.0.1', 8080, '\n', 2))
        executors_list.append(executor.submit(connect, '127.0.0.1', 8080, '\n', 2))

    for x in executors_list:
        results.append(x.result())
    assert (False not in results) == True

def test_invalid_input():
    assert connect(host, port, "scooby_dooby_do|scooby", wait_time) == 'ERROR\n'

def test_index_ok():
    assert connect(host, port, "index|Apache2", wait_time) == 'OK\n'
    connect(host, port, "remove|Apache2", wait_time)

def test_remove_ok():
    connect(host, port, "index|Apache2", wait_time) == 'OK\n'
    assert connect(host, port, "remove|Apache2", wait_time) == 'OK\n'

def test_query_ok():
    connect(host, port, "index|Apache2", wait_time)
    assert connect(host, port, 'query|Apache2', wait_time) == 'OK\n'
    connect(host, port, "remove|Apache2", wait_time)

def test_remove_fail():
    connect(host, port, "index|cloog|gmp,isl,pkg-config")
    assert connect(host, port, "remove|gmp", wait_time) == 'FAIL\n'
    connect(host, port, "remove|cloog")
    connect(host, port, "remove|gmp")
    connect(host, port, "remove|isl")
    connect(host, port, "remove|pkg-config")

def test_query_fail():
    connect(host, port, "remove|Apache2", wait_time)
    assert connect(host, port, "query|Apache2", wait_time) == 'FAIL\n'
