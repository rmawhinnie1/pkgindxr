""" Methods availble for use in package indexer tool"""
import os
import json
from json import JSONDecodeError
from logger import LOGGER


class METHODS:
    """
    Methods available for use in package indexer tool

    Args:
        log_level: Set logging level
        json_file: Location of json file to be used to store packages data
    """

    def __init__(self, log_level="INFO", json_file="pkgs.json"):
        self.log = LOGGER(log_level=log_level)
        self.json_file = json_file
        self.pkgs_list = self._read_pkgs()

    def index(self, package, dependencies=None):
        """
        Index target package and store dependencies

        Args:
            package: the name of the package to index
            dependencies: a list of dependencies for target package
        """
        self.log.info(f"Indexing {package} started")
        if dependencies:
            for pkg in dependencies:
                self.index(pkg)
        entry = {package: {"indexed": 1, "dependencies": dependencies}}
        self.log.debug(f"writing {entry} to index")
        self.pkgs_list.update(entry)
        self._write_pkgs()
        return "OK"

    def remove(self, package):
        """
        remove target package from index

        Args:
            package: the name of the package to remove
        """
        self.log.info(f"removing {package} started")
        if package not in self.pkgs_list:
            self.log.info(f"{package} not found nothing to remove")
            return "OK"
        if self.pkgs_list[package].get("indexed") == 0:
            self.log.info(f"{package} not indexed cleared for removal")
            self.pkgs_list.pop(package)
            self._write_pkgs()
        for pkg in self.pkgs_list:
            if not self.pkgs_list[pkg][
                    "dependencies"
            ] is None and package in self.pkgs_list[pkg].get("dependencies", [None]):
                return "FAIL"
        self.log.info(f"{package} cleared for removal")
        self.pkgs_list.pop(package)
        self._write_pkgs()
        return "OK"

    def query(self, package):
        """
        query index status of target package

        Args:
            package: the name of the package to remove
        """
        self.log.info(f"querying {package} started")
        if package not in self.pkgs_list:
            self.log.info(f"{package} not in Index")
            return "FAIL"
        if self.pkgs_list[package].get("indexed", 0) != 1:
            self.log.info(f"{package} not indexed")
            return "FAIL"
        self.log.info(f"{package} in index and indexed")
        return "OK"

    def _write_pkgs(self):
        self.log.debug(f"Updating Index")
        with open(self.json_file, "w") as file_handle:
            json.dump(self.pkgs_list, file_handle, indent=2)

    def _read_pkgs(self):
        self.log.debug(f"Reading Index")
        try:
            with open(self.json_file, "r") as file_handle:
                self.log.debug(f"Reading {self.json_file} successfull")
                return json.loads(file_handle.read())
        except JSONDecodeError:
            self.log.warning(f"Error found in {self.json_file}, recreating")
            os.rename(self.json_file, self.json_file + ".bad")
            with open(self.json_file, "w") as file_handle:
                json.dump({}, file_handle, indent=2)
            return {}
        except FileNotFoundError:
            self.log.warning(f"{self.json_file} not found creating")
            with open(self.json_file, "w") as file_handle:
                json.dump({}, file_handle, indent=2)
            return {}
