import selectors
import socket
import types
import sys
import os
import argparse
from logger import LOGGER
from methods import METHODS


class PKGINDXR:
    """
    Package indexer tool, used to store packages, and their dependencies

    Args:
        host: Hostname to be used for socket listening
        port: Port to be used for listening
        timeout: Time to keep socket open waiting for connection, default None
        log_level: Logging level, default INFO
    """

    def __init__(self, host, port, timeout=None, log_level="INFO"):
        # configure logging
        self.log_level = log_level
        self.log = LOGGER(log_level=log_level)

        # Configure Socket
        self.sel = selectors.DefaultSelector()
        self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.lsock.bind((host, port))
        self.lsock.listen()
        self.lsock.setblocking(False)
        self.sel.register(self.lsock, selectors.EVENT_READ, data=None)
        self.timeout = timeout

        self.log.info(f"listening on {host} {port}")

    def accept_wrapper(self, sock):
        conn, addr = sock.accept()
        self.log.info(f"incoming connection from {addr}")
        conn.setblocking(False)
        data = types.SimpleNamespace(addr=addr, inb=b"", outb=b"")
        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        self.sel.register(conn, events, data=data)

    def service_connection(self, key, mask):
        sock = key.fileobj
        data = key.data
        if mask & selectors.EVENT_READ:
            recv_data = sock.recv(2048)

            if recv_data:
                data.outb += recv_data

        if mask & selectors.EVENT_WRITE:
            if data.outb:
                rqst = ""
                try:
                    self.log.info(f"received {rqst} from {data.addr}")
                    rqst = data.outb.decode("utf-8:").strip("\n").strip("\r")
                except UnicodeDecodeError:
                    self.log.info(f"Non unicode input recieved from {data.addr}")
                    self._close_connection(data.addr, sock)
                if self._validate_request(rqst):
                    action = rqst.split("|")[0].upper()
                    pkg = rqst.split("|")[1].lower()
                    if len(rqst.split("|")) > 2 and rqst.split("|")[2] != "":
                        dpnds = rqst.split("|")[2].split(",")
                    else:
                        dpnds = []

                    method = METHODS(log_level=self.log_level)

                    if action == "INDEX":
                        status = method.index(package=pkg, dependencies=dpnds)
                        sock.send((status + "\n").encode())

                    elif action == "REMOVE":
                        status = method.remove(package=pkg)
                        sock.send((status + "\n").encode())

                    elif action == "QUERY":
                        status = method.query(package=pkg)
                        sock.send((status + "\n").encode())

                else:
                    self.log.info(f"Invalid data recieved from {data.addr}")
                    sock.send("ERROR\n".encode())

                self._close_connection(data.addr, sock)

    def run(self):
        try:
            while True:
                events = self.sel.select(timeout=self.timeout)
                for key, mask in events:
                    if key.data is None:
                        self.accept_wrapper(key.fileobj)
                    else:
                        self.service_connection(key, mask)
        except KeyboardInterrupt:
            self._close_socket()
            sys.exit()

    def _validate_request(self, data):
        if len(data.split("|")) <= 1:
            return False
        if data.split("|")[0].upper() not in ["INDEX", "REMOVE", "QUERY"]:
            return False
        if data[-2:] != "\\n":
            return True
        return True

    def _close_connection(self, addr, sock):
        self.log.info(f"closing connection {addr}")
        self.sel.unregister(sock)
        sock.close()

    def _close_socket(self):
        self.log.info("Closing Socket")
        self.sel.close()
        try:
            self.lsock.shutdown(socket.SHUT_RDWR)
        except OSError:
            self.lsock.close()


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="package index paramaters")
    PARSER.add_argument(
        "--host",
        type=str,
        default=os.environ.get("PKGINDXR_HOST", "127.0.0.1"),
        help="Host to set for Package Indexer",
    )
    PARSER.add_argument(
        "--port",
        type=int,
        default=os.environ.get("PKGINDXR_PORT", 8080),
        help="Port to set for Package Indexer",
    )
    PARSER.add_argument(
        "--timeout",
        type=int,
        default=os.environ.get("PKGINDXR_TIMEOUT", None),
        help="Timeout to set for Package Indexer",
    )
    PARSER.add_argument(
        "--log_level",
        choices=['INFO', 'WARNING', 'DEBUG'],
        default=os.environ.get("PKGINDXR_LOG_LEVEL", "INFO"),
        help="Log Level to set for Package Indexer",
    )
    ARGS = PARSER.parse_args()

    START = PKGINDXR(ARGS.host, ARGS.port, ARGS.timeout, ARGS.log_level)
    START.run()
