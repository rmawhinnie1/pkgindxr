'''Logging Module'''
import logging
import sys


class LOGGER:
    '''
    Module to handle all logging tasks

    Args:
        log_level: set log level for logging runtime
    '''
    def __init__(self, log_level="INFO"):
        # configure logging
        logger = logging.getLogger("package_indexer")
        if logger.hasHandlers():
            logger.handlers.clear()
        logger.setLevel(log_level)
        logchannel = logging.StreamHandler(sys.stdout)
        logchannel.setLevel(log_level)
        formatter = logging.Formatter("%(asctime)s %(name)s %(levelname)s %(message)s")
        logchannel.setFormatter(formatter)
        logger.addHandler(logchannel)
        self.log = logger

    def warning(self, msg):
        '''
        Log warning messages

        Args:
            msg: the log message to log
        '''
        self.log.warning(msg.strip("\n"))

    def info(self, msg):
        '''
        Log warning messages

        Args:
            msg: the log message to log
        '''
        self.log.info(msg.strip("\n"))

    def debug(self, msg):
        '''
        Log warning messages

        Args:
            msg: the log message to log
        '''
        self.log.debug(msg.strip("\n"))
